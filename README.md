# A script for monitoring systemd services

## Overview
The idea is to gather and display information about the state of a couple of services, witch is get from the __systemd status__ command.  Thoes informations are displayed in a table.
The services information displayed are :
* the __name__ of the service
* his __state__ (active/inactive)
* his __running time__
* the possible __trigger__ (from *__.timer__* systemd services)

## Configuration
There is a configuration file in the repo called __monitor.conf__.
The only thing to add is the list of the systemd unit you want to monitor.
